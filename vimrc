set nocompatible
" vim: foldmethod=marker :
let mapleader = "ç"

call plug#begin('~/.vim/plugged')
Plug 'junegunn/goyo.vim'
Plug 'itchyny/lightline.vim'
Plug 'godlygeek/tabular'
Plug 'tpope/vim-fugitive'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-sensible'

" Color schemes {{{
Plug 'morhetz/gruvbox'
Plug 'raphamorim/lucario'
Plug 'haishanh/night-owl.vim'
Plug 'arcticicestudio/nord-vim'
Plug 'mhartington/oceanic-next'
Plug 'joshdick/onedark.vim'
Plug 'sonph/onehalf', { 'rtp': 'vim' }
Plug 'drewtempelmeyer/palenight.vim'
Plug 'NLKNguyen/papercolor-theme'
Plug 'kyoz/purify', { 'rtp': 'vim' }
Plug 'Rigellute/rigel'
Plug 'vim-scripts/summerfruit256.vim'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'tyrannicaltoucan/vim-quantum'
" }}}
call plug#end()

set encoding=utf-8
set modeline
set tabstop=4 shiftwidth=4 softtabstop=4 expandtab

set diffopt+=algorithm:patience,indent-heuristic

autocmd BufWritePre * :%s/\s\+$//e

" FileType augroups {{{
runtime ftplugin/man.vim
augroup ft_help
    autocmd!
    autocmd FileType help,man nnoremap <buffer> j <c-e>
    autocmd FileType help,man nnoremap <buffer> k <c-y>
    autocmd FileType help,man setlocal nocursorline
augroup END

augroup ft_spell
    autocmd!
    autocmd FileType gitcommit,markdown setlocal spell spelllang=en_us
augroup END
" }}}

set termguicolors
colorscheme nord
let g:lightline   = { 'colorscheme': 'nord' }

let g:goyo_linenr = 1
let g:goyo_width  = 80

