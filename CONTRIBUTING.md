## Repository

Don't rebase commits that have been pushed and merged. Rebasing changes commit dates, which can mess git log.

Force pushing is allowed on branches that have not yet been merged.

Revise commits and changes before merging. If possible, wait a few hours or a day before merging. Resting before merging usually allows further insights and reduce typos on commit message.

Double-check if documentation should be updated before merging.

## Commit Message Guidelines

Guidelines based on [Angular](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines) and [Linux Kernel](https://github.com/torvalds/linux/commits/master).

<details>
<summary>Message must have the following structure:</summary>

```
<type>[.<type>]+: <title> (#<issue_number>)
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

</details>

#### Type

Use the following:

- **fix:** bug fix.
- **doc:** documentation only changes
- **feat:** a **new** feature, something that adds behavior, a new configuration
- **mod:** a change to an **existing** feature that **is not a bug fix;** something that changes behavior. Example: changing color scheme in `.vimrc`. **Explain in the body why the change is being made.**
- **ci:** CI related stuff
- **setup:** alias for `rakefile` and `playbook.yml`; use it instead of their filenames
- **style:** changes that do not affect the meaning of the code (adding vim modeline, white-space, formatting, missing semi-colons, typos, etc)
- **refactor:** changes to code that are not `mod`, `feat` nor `fix`

<details>
<summary>Types can be mixed as if they are labels</summary>

- `fix.setup: Fix a bug in rakefile or playbook.yml`
- `mod.vimrc: Update colors`
- `style.doc: Fix typos`
- `ci.feat: Implement a new job`
- `mod.ci.rakefile-checker: Improve rakefile-checker job performance`
- `vimrc.feat: Delete trailing white-space on save`

</details>

Generally, use `fix`, `mod` and `style` as prefixes and `feat` as suffix. This way they read more like English.

File names and CI jobs or stages can also be used as labels/types. However, for `rakefile` and `playbook`, use `setup` instead of file name: these two should be treated interchangeably.

As always, **use common sense.**

#### Title

Brief description of the changes. Use common sense.

<details>
<summary>Some suggestions</summary>

- when dealing with `feat`, describe the implemented feature instead of changes made to the files:

  - `vim.feat: Delete trailing white-spaces on save`

- `style`: describe changes to the file:

  - `style.contrib: Add vim modeline`

- `fix`: attempt to hint at the solution: 

  - `fix.ci.playbook-checker: Install missing dependencies using before_script`

- `mod`: little bit like `feat`; when pertinent, use the keyword `instead of`

</details>

Since title is a title, don't use final period. Capitalize first letter, following `Merge` default, and use proper capitalization for tool names: <p>`setup.feat: Uninstall and lock PackageKit`</p>

#### Body

Give proper details when pertinent, specially when using `mod`. Describe why the change was made, the expected new behavior, how a bug why fixed and what was causing the bug.

#### Footer

Used mainly for `Implements/See #<issue_number>`.

## Documentation Guidelines

Provide examples and counterexamples. Provide rational and reasons.

When updating some piece of documentation, either use strike-through for small sections or use expand/collapse for larger sections. These are clear visual indicators that the documentation has been updated. Remove these sections only after some time has passed.

Also use expand/collapse for better organization. Add vim folds to collapsed sections as well.

## Issues Guidelines

Treat issues also as documentation.

Provide expected results vs. actual results.

Follow labeling scheme used in commit messages, but don't use them in issue titles. Instead, properly add labels to issues in GitLab.

Periodically refactor closed issues that could use better wording. It's good training and serves also as good examples.

<details>
<summary>Title examples</summary>

- Bug issue: phrase like a broken expectation:

  - ... isn't working
  - ... is currently failing
  - ... is to slow
  - foo is messing bar
  - ... is not reliable
  - didn't work as expected
  - Phasing like a broken expectation *should* pair well with `fix` title convention, since it hints at the solution and shows the bug title via #\<issue_numer>.

- Feature issue: phrase like a desired expectation:

  - ... should do this
  - ... must do that

- `mod` related issues: preferably, phrase like a bug issue. Feature style can also be used in some cases, such as changes to color-scheme.

</details>

